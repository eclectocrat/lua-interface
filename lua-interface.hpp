/*  lua-interface

    A set of utilities to more easily interface C++ code with Lua.
*/

#ifndef ec_lua_util_hpp
#define ec_lua_util_hpp

#include <algorithm>
#include <iostream>
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
#include <string>
#include <tuple>

namespace ec {
namespace lua {

inline void check_is_table (lua_State * ls, int n) {
    if(lua_type(ls, n) != LUA_TTABLE) {
        std::cerr << "Cpp function expected a table, got a "
                  << lua_typename(ls, n) << std::endl;
        luaL_checktype(ls, n, LUA_TTABLE); // force error
    }
}

inline bool has_field(lua_State *ls, int n, const char * name) {
    lua_getfield(ls, n, name);
    const bool has_it = not lua_isnil(ls, -1);
    lua_pop(ls, 1);
    return has_it;
}

template <typename T> inline T checked_get(lua_State * ls, int n) {
    check_is_table(ls, n);
    lua_getfield(ls, n, "core");
    auto p = (T)checked_get<void*>(ls, -1);
    lua_pop(ls, 1);
    return p;
}

template <> inline int checked_get(lua_State *ls, int n) {
    luaL_checktype(ls, n, LUA_TNUMBER);
    return (int)(lua_tointeger(ls, n));
}

template <> inline float checked_get(lua_State *ls, int n) {
    luaL_checktype(ls, n, LUA_TNUMBER);
    return (float)(lua_tonumber(ls, n));
}

template <> inline bool checked_get(lua_State *ls, int n) {
    luaL_checktype(ls, n, LUA_TBOOLEAN);
    return lua_toboolean(ls, n);
}

template <> inline std::string checked_get(lua_State *ls, int n) {
    luaL_checktype(ls, n, LUA_TSTRING);
    return lua_tostring(ls, n);
}

template <> inline void *checked_get(lua_State *ls, int n) {
    if (lua_type(ls, n) != LUA_TUSERDATA and
        lua_type(ls, n) != LUA_TLIGHTUSERDATA) {
        // force informative error
        luaL_checktype(ls, n, LUA_TUSERDATA);
    }
    return lua_touserdata(ls, n);
}

template<typename T>
T param(lua_State * ls, const char * name, int index=2) {
    lua::check_is_table(ls, index);
    lua_getfield(ls, index, name);
    auto parm = lua::checked_get<T>(ls, -1);
    lua_pop(ls, 1);
    return parm;
}


/// \brief Prints the current stack contents to stdout.
///
/// \param L Lua state to dump contents of.
/// 
inline void dump_stack(lua_State *L) {
    int top = lua_gettop(L);
    for (int i = 1; i <= top; i++) {
        int t = lua_type(L, i);
        switch (t) {
        case LUA_TSTRING:
            printf("`%s'", lua_tostring(L, i));
            break;
        case LUA_TBOOLEAN:
            printf(lua_toboolean(L, i) ? "true" : "false");
            break;
        case LUA_TNUMBER:
            printf("%g", lua_tonumber(L, i));
            break;
        default:
            printf("%s", lua_typename(L, t));
            break;
        }
        printf("  ");
    }
    printf("\n");
}

/// \brief  Registers a new Lua `class` (informal object protocol) in the global
///         namespace of a Lua state.
///
/// \param lua      State to register class in.
/// \param name     Name of class to register, as you wish to use it in Lua.
/// \param mt_name  Metatable name to use when creating the new class's
///                 metatable, this can be less user friendly.
///                 Postcondition: luaL_getmetatable(lua, mt_name) != nil
/// \param object_interface The interface of instances of this class, as per
///                         Lua C API standards.
/// \param class_interface  Interface of class table, i.e. the static functions,
///                         usually containing at least a `create` or `new` or
///                         similar instance factory.
///
template <typename OI, typename CI>
inline void register_class(lua_State *lua, std::string const &name,
                           const char *mt_name, OI object_interface,
                           CI class_interface) {
    luaL_newmetatable(lua, mt_name);        // {mt}
    lua_pushstring(lua, "__index");         // {mt}, "__index"
    lua_pushvalue(lua, -2);                 // {mt}, "__index", {mt}
    lua_settable(lua, -3);                  // {mt}
    for (const luaL_Reg *l = object_interface; l->name; l++) {
        lua_pushcclosure(lua, l->func, 0);  // {mt}, {clos}
        lua_setfield(lua, -2, l->name);     // {mt}
    }
    lua_pop(lua, 1);                        // [nothing!]
    luaL_newlib(lua, class_interface);      // {funcs}
    luaL_setmetatable(lua, mt_name);
    lua_setglobal(lua, name.c_str());       // [nothing!]
}

template<typename T>
T * create_object(lua_State * ls, const char * mt) {
    lua_newtable(ls);                                   // {t}
    luaL_getmetatable(ls, mt);                          // {t}, {mt}
    lua_setmetatable(ls, -2);                           // {t}
    auto obj = new (lua_newuserdata(ls, sizeof(T))) T;  // {t}, ud
    (void) obj;
    luaL_getmetatable(ls, mt);                          // {t}, ud, {mt}
    lua_setmetatable(ls, -2);                           // {t}, ud
    lua_pushstring(ls, "core");                         // {t}, ud, "core"
    lua_insert(ls, -2);                                 // {t}, "core", ud
    lua_settable(ls, -3);                               // {t}
    return obj;
}

template<typename T>
T * clone_object(lua_State * ls, const char * mt, T * src) {
    lua_newtable(ls);                                       // {t}
    luaL_getmetatable(ls, mt);                              // {t}, {mt}
    lua_setmetatable(ls, -2);                               // {t}
    auto obj = new (lua_newuserdata(ls, sizeof(T))) T(*src);// {t}, ud
    (void) obj;
    luaL_getmetatable(ls, mt);                              // {t}, ud, {mt}
    lua_setmetatable(ls, -2);                               // {t}, ud
    lua_pushstring(ls, "core");                             // {t}, ud, "core"
    lua_insert(ls, -2);                                     // {t}, "core", ud
    lua_settable(ls, -3);                                   // {t}
    return obj;
}

template<typename T>
T * destroy_object(lua_State * ls, const char * mt) {
    if (not lua_istable(ls, -1)) {
        luaL_checkudata(ls, -1, mt);
        auto obj = (T*)lua_touserdata(ls, -1);
        obj->~T();
        return obj;
    } else {
        return nullptr;
    }
}

} // END namespace lua
} // END namespace ec
#endif
