# lua-interface
A set of utilities to easily export C++ classes and functions to Lua.

Copyright(C) Jeremy Jurksztowicz 2020. All rights reserved.

### See lua-interface.hpp for details and documentation.
